import React, { Component } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
var _ = require('lodash');
//import logo from './logo.svg';
//import './App.css';


class SummonerNameEntry extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: ''
    }
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  render() {
    //const { classes } = this.props;
    return (
      <div>
        <TextField
          id="standard-name"
          label="Username"
          //className={classes.textField}
          value={this.state.username}
          onChange={this.handleChange('username')}
          margin="normal"
        />
        <br/>
        <Button variant="contained" color="primary" 
          onClick={()=> this.props.onSearch(this.state.username)}>
          See matches
        </Button>
      </div>
    );
  }
}

class Match extends Component {
  constructor(props) {
    super(props);
    this.state = {
      matchDetails: {}
    }
  }

  async componentWillMount() {
    //fetch details for this match
    const match = this.props.match;
    const matchDetails = await getMatchDetails(match.gameId);
    console.log(matchDetails);
    this.setState({
      matchDetails: matchDetails
    })
  }

  render() {
    const summoner = this.props.summoner
    const match = this.props.match
    const matchDetails = this.state.matchDetails;
    if (matchDetails) {
      var gameDuration = (matchDetails.gameDuration / 60.0).toFixed(2);
      var participant = getMatchParticipant(summoner, matchDetails);
      if (participant) {
        var win = participant.stats.win
        var winText = win ? "Won" : "Lost";
        console.log(winText);
        var kda = `${participant.stats.kills}/${participant.stats.deaths}/${participant.stats.assists}`
        var totalMinionsKilled = participant.stats.totalMinionsKilled;
        var minionsKilledPerMinute = (totalMinionsKilled / (matchDetails.gameDuration / 60.0)).toFixed(2);
      }
    }


    console.log(participant);
    return (
      <Card>
      <CardContent>
      <Typography variant="body1" gutterBottom>
        SUMMONER NAME: {summoner.name}<br/>
        OUTCOME: {winText}<br/>
        DURATION: {gameDuration} seconds<br/>
        K/DA/: {kda}<br/>
        CREEP SCORE: {totalMinionsKilled}<br/>
        CREEP SCORE PER MINUTE: {minionsKilledPerMinute}<br/>
      </Typography>
      </CardContent>
    </Card>
    )
  }
}

class League extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search_summoner_name: '',
      summoner: {},
      matches: []
    }
  }

  async searchHandler(username) {
    const matchlistsJson = await getMatchlistsJson(username);
    const matches = matchlistsJson.matchlist.matches;
    const summoner = matchlistsJson.summoner;
    //console.log(matches);
    //console.log(summoner);
    this.setState ( {
      summoner: summoner,
      matches: matches
    })
  }

  render() {
    const summoner = this.state.summoner;
    var matches = this.state.matches || [];
    return (
      <div>
        <Typography variant="h3" gutterBottom>
          league stats
        </Typography>
        <Typography variant="subtitle1" gutterBottom>
          for na1 server
        </Typography>
        <SummonerNameEntry onSearch={this.searchHandler.bind(this)}/>
        {   
          matches.map(function (match, index) {
            return (
              <li key={match.gameId}>
                <Match summoner={summoner} match={match}/>
              </li> 
            )
          })
        }        
      </div>
    );
  }
}

class App extends Component {
  render() {
    return (
      <div className="App">
      <Grid
        container
        spacing={8}
      >
        <Grid item xs={12}>
          <League/>
        </Grid>
      </Grid>
      </div>
    );
  }
}

export default App;

//helper functions

async function getMatchlistsJson(username) {
  let url = `https://lh42n85uxh.execute-api.us-east-1.amazonaws.com/dev/matchlists/${username}`;
  let response = await fetch(url);
  let json = await response.json();
  //console.log(json);
  return json;
}

async function getMatchDetails(gameId) {
  let url = `https://lh42n85uxh.execute-api.us-east-1.amazonaws.com/dev/matches/${gameId}`;
  let response = await fetch(url);
  let json = await response.json();
  //console.log(json);
  return json.match;
}

/*function getMatchOutcome(summoner, matchDetails) {

    if (_.isEmpty(summoner) || _.isEmpty(matchDetails)) {
      return;
    }

    console.log(matchDetails);
    try {

      const participant = getMatchParticipant(summoner, matchDetails);
      let teamId = participant.teamId;

      //get our summoner's team
      let team = matchDetails.teams.find(function (team) {
        return team.teamId === teamId;
      })  

      return team.win;
    } catch (error) {
      console.log(error);
    }
}*/

function getMatchParticipant(summoner, matchDetails) {

  if (_.isEmpty(summoner) || _.isEmpty(matchDetails)) {
    return;
  }

  console.log(summoner);
  console.log(matchDetails);

  try {
      //get our summoner's participant identity
      let participantIdentity = matchDetails.participantIdentities.find (function (participantIdentity) {
        return participantIdentity.player.accountId === summoner.accountId;
      });

      //get participant id
      const participantId = participantIdentity.participantId;

      //get participant
      let participant = matchDetails.participants.find(function (participant) {
        return participant.participantId === participantId;
      });

      return participant;
  } catch (error) {
    console.log(error);
  }
}